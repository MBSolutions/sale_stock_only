# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .channel import *
from .sale import *

def register():
    Pool.register(
        Sale,
        SaleChannel,
        SaleLine,
        module='sale_stock_only', type_='model')
