# -*- coding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pyson import Eval, If, Bool
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction

__all__ = ['Sale', 'SaleLine']
__metaclass__ = PoolMeta


class Sale:
    __name__ = 'sale.sale'

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls.lines.context['current_channel'] = Eval('channel')


class SaleLine:
    __name__ = 'sale.line'

    show_stock_only = fields.Boolean('Only products with stock',
        help='Only show products that are available in stock.')

    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()
        cls.product.domain.append(If(Bool(Eval('show_stock_only')),
                ['OR',
            ('quantity', '>', 0.0),
            ('forecast_quantity', '>', 0.0)],
            [],
            ))
        cls.product.depends.append('show_stock_only')

    @staticmethod
    def default_show_stock_only():
        Channel = Pool().get('sale.channel')
        channel = Transaction().context.get('current_channel')
        if channel:
            return Channel(channel).show_stock_only
